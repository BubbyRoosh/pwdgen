use std::{fs, process};

use arg_dot_h::*;

// Don't wanna use an external lib lol
extern "C" {
    fn srand();
    fn rand() -> usize;
}

fn rand_min_max(min: usize, max: usize) -> usize {
    unsafe {
        min + rand() % (min + max)
    }
}

fn main() {
    unsafe {
        srand();
    }

    let help = |argv0| {
        println!("{} [-d dict] [-l length] [-m len] [-nqs]

Generate passwords like that one xkcd comic

Copyright (c) 2021 BubbyRoosh

-d  The dictionary to use. Defaults to /usr/share/dict/words.
-l  The length (in words). Defaults to 4.
-m  The max length a word from the dictionary can be. Defaults to 5.
-n  Print without a new line.
-q  Remove quote (') characters if there are any.
-s  Separate each word by a space.
", argv0);
        process::exit(1);
    };

    let mut argv0 = String::new();
    let mut dictpath = "/usr/share/dict/words".to_string();
    let mut length = 4;
    let mut maxwordlength = 5;
    let mut nonewline = false;
    let mut removequotes = false;
    let mut spaces = false;
    argbegin! {
        &mut argv0,
        'd' => dictpath = eargf!(help(argv0.clone())),
        'l' => length = eargf!(help(argv0.clone())).parse::<usize>().unwrap(),
        'm' => maxwordlength = eargf!(help(argv0.clone())).parse::<usize>().unwrap(),
        'n' => nonewline = true,
        'q' => removequotes = true,
        's' => spaces = true,
        _ => help(argv0.clone())
    }

    let dictionary = match fs::read_to_string(dictpath) {
        Ok(c) => c.lines().filter(|l| l.len() <= maxwordlength).map(|l| l.to_owned()).collect::<Vec<_>>(),
        Err(e) => {
            eprintln!("Error parsing dictionary: {}", e);
            process::exit(1);
        }
    };

    let mut password = String::new();

    for _ in 0..length {
        password += &dictionary[rand_min_max(0, dictionary.len())];
        if spaces {password += " "}
    }

    password = password.trim().to_string();
    if removequotes {
        password = password.replace("'", "").to_string();
    }

    if nonewline {
        print!("{}", password);
    } else {
        println!("{}", password);
    }
}
