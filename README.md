# pwdgen

Generate passwords like that one xkcd comic

Copyright (c) 2021 BubbyRoosh

-d  The dictionary to use. Defaults to /usr/share/dict/words.

-l  The length (in words). Defaults to 4.

-m  The max length a word from the dictionary can be. Defaults to 5.

-n  Print without a new line.

-q  Remove quote (') characters if there are any.

-s  Separate each word by a space.
